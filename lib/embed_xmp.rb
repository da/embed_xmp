# frozen_string_literal: true

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

require 'embed_xmp/xmp'
require 'embed_xmp/jpeg'
require 'embed_xmp/png'
require 'embed_xmp/webp'
require 'embed_xmp/svg'
