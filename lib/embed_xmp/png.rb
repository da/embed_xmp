# frozen_string_literal: true

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

require 'digest/crc32'

require 'embed_xmp/image_file'

class EmbedXMP
  # PNG images
  class PNG < ImageFile
    PNG_SIGNATURE = "\x89PNG\r\n\x1A\n".b.freeze
    PNG_HEADER = 'IHDR'
    PNG_IMAGE_END = 'IEND'

    XMP_CHUNK_SIG = "iTXtXML:com.adobe.xmp\0\0\0\0\0".b.freeze

    # Join an XMP sidecar file into a PNG image file.
    def join_sidecar(sidecar_file, xpacked: false)
      check_file_signatures
      remove_xmp

      sidecar = read_io_or_string(sidecar_file)
      xmp_chunk = create_xmp_itxt(sidecar, xpacked)

      insert_into_file(find_xmp_insertion_offset, xmp_chunk)
    end

    # Quick and dirty test to see if +png_data+ is a PNG image file
    def check_file_signatures
      raise 'NoPNGSignature' if PNG_SIGNATURE != @image_data[0..7]
      raise 'NoPNGEndOfFile' if PNG_IMAGE_END != @image_data[-8..-5]
    end

    def chunk(offset)
      chunk_length = @image_data[offset, 4].b.unpack1('N') + 12

      raise 'ChunkLongerThanFile' if offset + chunk_length > @image_data.length

      chunk_id = @image_data[offset + 4, 4]

      data = @image_data[offset + 8, chunk_length]

      [chunk_id, chunk_length, data]
    end

    # Return chunk at +offset+ from the beginning of the file.
    def remove_chunk(offset)
      _, chunk_length, = chunk(offset)

      @image_data.slice!(offset, chunk_length)
    end

    # rubocop: disable Metrics/MethodLength
    def remove_xmp
      offset = PNG_SIGNATURE.length
      while offset < @image_data.length
        chunk_id, chunk_length, = chunk(offset)

        break if [PNG_IMAGE_END, nil].include?(chunk_id)

        if chunk_contains_xmp(offset)
          remove_chunk(offset)
          next
        end

        offset += chunk_length
      end
    end
    # rubocop: enable Metrics/MethodLength

    private

    def find_xmp_insertion_offset
      offset = PNG_SIGNATURE.length
      cursor = offset
      while offset < @image_data.length
        chunk_id, chunk_length, = chunk(offset)

        break if [PNG_IMAGE_END, nil].include?(chunk_id)

        if PNG_HEADER == chunk_id
          cursor = offset + chunk_length
        end

        offset += chunk_length
      end

      cursor
    end

    def chunk_contains_xmp(offset)
      XMP_CHUNK_SIG == @image_data[offset + 4, XMP_CHUNK_SIG.length + 3].b
    end

    def new_chunk(chunk_id, chunk_data)
      length = [chunk_data.length].pack('N')

      checksum = [Digest::CRC32.checksum(chunk_id + chunk_data)].pack('N')

      length.b + chunk_id + chunk_data + checksum
    end

    def create_xmp_itxt(xmp_data, xpacked)
      chunk_id = 'iTXt'
      chunk_data = ("XML:com.adobe.xmp\0\0\0\0\0" +
                   EmbedXMP::XMP
                   .new(xmp_data, writable: false, xpacked: xpacked)
                   .to_s.gsub("\0", ' ')).b

      new_chunk(chunk_id, chunk_data)
    end
  end
end
