# frozen_string_literal: true

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

require 'nokogiri'

require 'embed_xmp/image_file'

class EmbedXMP
  # SVG images
  class SVG < ImageFile

    SVG_NAMESPACE = 'http://www.w3.org/2000/svg'

    def initialize(input_file)
      file = read_io_or_string(input_file)
      svg = Nokogiri::XML(file) do |conf|
        conf.options = Nokogiri::XML::ParseOptions::NOBLANKS
      end

      raise 'NoSVGnamespace' if SVG_NAMESPACE != svg.root.namespace.href

      @image_file = svg
    end

    # Join an XMP sidecar file into an SVG image file.
    def join_sidecar(sidecar_file, xpacked: false)
      remove_xmp

      xmp_data = read_io_or_string(sidecar_file)
      insert_xmp_metadata_into_svg(create_metadata(xmp_data, xpacked))
    end

    # Removes XMP metadata in /svg/metadata/rdf:RDF. Will only detect XMP data
    # wrapped in an XPACKET processing instruction.
    def remove_xmp
      @image_file.root.xpath('//svg:metadata', 'svg' => 'http://www.w3.org/2000/svg')
        .each do |svg_metadata|
        xml_pi = svg_metadata.xpath('processing-instruction()')

        if xml_pi.empty? ||
           xml_pi.first.content != EmbedXMP::XMP::XPACKET_PDATA
          next
        end

        svg_metadata.remove
      end
    end

    def write(output_file)
      data = @image_file.to_xml(encoding: 'utf-8', indent: 2)
      super(output_file, data: data)
    end

    private

    def insert_xmp_metadata_into_svg(xmp)
      # insert <metadata> after <title> and <desc> or at top
      insert_position = 0
      2.times do
        node_name = @image_file.root.children[insert_position].name
        insert_position += 1 if %w[title desc].include?(node_name)
      end

      @image_file.root.children[insert_position].add_previous_sibling(xmp)
    end

    def create_metadata(xmp_data, xpacked)
      xpacked_xmp = EmbedXMP::XMP
                    .new(xmp_data, writable: true, xpacked: xpacked).to_s

      data = Nokogiri::XML("<metadata>\n#{xpacked_xmp}\n</metadata>") do |conf|
        conf.options = Nokogiri::XML::ParseOptions::NOBLANKS
      end

      data.root
    end
  end
end
