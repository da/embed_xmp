# frozen_string_literal: true

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

require 'embed_xmp/image_file'

class EmbedXMP
  # JPEG File Interchange Format (container format for JPEG)
  class JFIF < ImageFile
    JFIF_SOI = "\xFF\xD8".b
    JFIF_END = "\xFF\xD9".b

    # Check if file has JFIF markers.
    def check_file_markers
      raise 'NoJPEGStartOfFile' if JFIF_SOI != @image_data[0..1]
      raise 'NoJPEGEndOfFile' if JFIF_END != @image_data[-2..-1]
    end

    # Return segment at +offset+ from the beginning of the file.
    def segment(offset)
      marker = @image_data[offset, 2]
      length = @image_data[offset + 2, 2].b.unpack1('n') + 2

      raise 'SegmentExceedFileLength' if offset + length > @image_data.length

      data = @image_data[offset + 2, length]

      [marker, length, data]
    end

    # Remove the chunk at +offset+ from the beginning of the file.
    def remove_segment(offset)
      _, length, = segment(offset)

      @image_data.slice!(offset, length)
    end

    def new_segment(marker, data)
      raise 'SegmentMarkerNotTwoBytes' if marker.length != 2
      raise 'SegmentMarkerDoesNotBeginWithNullByte' if marker == '\b'.b

      length = [2 + data.length].pack('n')

      marker + length + data
    end
  end
end
