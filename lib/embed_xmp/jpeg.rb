# frozen_string_literal: true

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

require 'embed_xmp/jfif'

class EmbedXMP
  # JPEG images
  class JPEG < JFIF
    JPEG_AP1 = "\xFF\xE1".b
    JPEG_IMG = "\xFF\xDA".b
    JPEG_XMP = "http://ns.adobe.com/xap/1.0/\0".b
    JPEG_EXF = "Exif\0\0".b

    # Join an XMP sidecar file into the image file.
    def join_sidecar(sidecar_file, xpacked: false)
      check_file_markers
      remove_xmp

      sidecar = read_io_or_string(sidecar_file)
      xmp_chunk = create_xmp_segment(sidecar, xpacked)

      insert_into_file(find_xmp_insertion_offset, xmp_chunk)
    end

    def remove_xmp
      offset = JFIF_SOI.length
      while offset < @image_data.length
        marker, length, = segment(offset)

        break if [JPEG_IMG, JFIF_END, nil].include?(marker)

        if segment_is_app1_xmp(offset, marker)
          remove_segment(offset)
          next
        end

        offset += length
      end
    end

    private

    def find_xmp_insertion_offset
      offset = JFIF_SOI.length
      cursor = offset
      while offset < @image_data.length
        marker, length, = segment(offset)

        break if [JPEG_IMG, JFIF_END, nil].include?(marker)

        if segment_is_app1_exif(offset, marker)
          cursor = offset + length
          break
        end

        offset += length
      end

      cursor
    end

    def segment_is_app1_exif(off, seg)
      JPEG_AP1 == seg && @image_data[off + 4, JPEG_EXF.length].b == JPEG_EXF
    end

    def segment_is_app1_xmp(off, seg)
      JPEG_AP1 == seg && @image_data[off + 4, JPEG_XMP.length + 4].b == JPEG_XMP
    end

    def create_xmp_segment(xmp, xpacked)
      data = JPEG_XMP +
             EmbedXMP::XMP
             .new(xmp, writable: true, xpacked: xpacked)
             .to_s.gsub("\0", ' ')

      new_segment(JPEG_AP1, data.b)
    end
  end
end
