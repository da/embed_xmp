# frozen_string_literal: true

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

require 'nokogiri'

require 'embed_xmp/image_file'

class EmbedXMP
  # XMP sidecars
  class XMP
    XPACKET_PDATA = "begin=\"\uFEFF\" id=\"W5M0MpCehiHzreSzNTczkc9d\""
    XPACKET_START = "<?xpacket #{XPACKET_PDATA}?>"
    XPACKET_END_R = '<?xpacket end="r"?>'
    XPACKET_END_W = '<?xpacket end="w"?>'

    @xmp

    def initialize(xmp_data_blob, writable: false, xpacked: false)
      @xmp = xmp_str = xmp_xml_to_str(xmp_data_blob)

      @xmp = xpack_sidecar(xmp_str, writable: writable) unless xpacked
    end

    # Return XMP sidecar as +String+.
    def to_s
      xmp_xml_to_str(@xmp)
    end

    # Read XML-formatted XMP data into a format suitable for embedding.
    def xmp_xml_to_str(xmp_data)
      xmp = Nokogiri::XML(xmp_data) do |conf|
        conf.options = Nokogiri::XML::ParseOptions::NOBLANKS
      end

      raise 'XMPIsMalformedXML' unless xmp.errors.empty?

      xmp.to_xml(indent: 0,
                 encoding: 'utf-8',
                 save_with: Nokogiri::XML::Node::SaveOptions::NO_DECLARATION)
    end

    # Wrap XMP sidecar data in in an magic XML processing instruction (xpacket).
    # +writable+ is approperiate for file formats where the XMP data can be
    # modified in place by staying within the confines of the XPACKET. (E.g.
    # file formats with no chunk checksums.)
    def xpack_sidecar(xmp_data, writable: false)
      xpacket_end = writable ? XPACKET_END_W : XPACKET_END_R

      "#{XPACKET_START}\n#{xmp_data}\n#{xpacket_end}"
    end
  end
end
