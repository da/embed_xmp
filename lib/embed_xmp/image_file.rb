# frozen_string_literal: true

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

class EmbedXMP
  # Basic image file representation.
  class ImageFile
    @image_data

    def initialize(input_image)
      @image_data = read_io_or_string(input_image)
    end

    # Read from a readable +IO+ object or a +String+ (file path or a thing).
    def read_io_or_string(thing)
      if thing.respond_to?(:encoding)
        if File.exist?(thing)
          io = IO.binread(thing)
          io.advise(:random)
          return io
        end

        return thing
      elsif thing.respond_to?(:read)
        io = IO.binread(thing)
        io.advise(:random)
        return io
      end

      raise 'FileNotAnIOorString'
    end

    # Insert a +chunk+ of data at +file_offset+ from the start of +file_data+.
    def insert_into_file(offset, data)
      @image_data = @image_data[0..offset - 1] + data + @image_data[offset..-1]
    end

    # Write image to file (or return if argument is +nil+).
    def write(output_file, data: nil)
      data = @image_data if data.nil?
      return data if output_file.nil?

      written_bytes = 0
      File.open(output_file, 'wb') do |file|
        written_bytes = file.write(data)
      end

      written_bytes > 0 && written_bytes == data.b.length
    end
  end
end
