# frozen_string_literal: true

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

require 'embed_xmp/riff'

class EmbedXMP
  # WebP images
  class WebP < RIFF
    WEBP_HEAD = 'WEBP'
    VP8X_HEAD = 'VP8X'
    VP8F_HEAD = 'VP8 '
    VP8L_HEAD = 'VP8L'
    XMPS_HEAD = 'XMP '

    VP8X_XMP_FLAG = 0b00000100

    # Join an XMP sidecar into the image file.
    def join_sidecar(sidecar_file, xpacked: false)
      remove_xmp

      xmp_data = read_io_or_string(sidecar_file)
      upsert_xmp_chunk(create_xmp_chunk(xmp_data, xpacked))
    end

    def file_header
      riff_id, file_length, form_type = super

      raise 'NoWEBPHeader' if WEBP_HEAD != form_type

      [riff_id, file_length, form_type]
    end

    private

    # rubocop: disable Metrics/MethodLength
    def remove_xmp
      offset = 12
      while offset < @image_data.length
        chunk_id, length, = chunk(offset)

        break if chunk_id.nil?

        if XMPS_HEAD == chunk_id
          remove_chunk(offset)
          toggle_xmp_feature(xmp: false)
          break if offset + length >= @image_data.length

          next
        end

        offset += length
      end
    end
    # rubocop: enable Metrics/MethodLength

    def create_xmp_chunk(xmp_data, xpacked)
      data = EmbedXMP::XMP
             .new(xmp_data, writable: true, xpacked: xpacked)
             .to_s.gsub("\0", ' ').b

      # pad with a potentially useful space char instead of NULL at end
      if data.length.odd?
        data[EmbedXMP::XMP::XPACKET_END_W] =
          " #{EmbedXMP::XMP::XPACKET_END_W}"
      end

      new_chunk(XMPS_HEAD, data)
    end

    def upsert_xmp_chunk(xmp_chunk)
      remove_xmp
      @image_data += xmp_chunk
      update_file_length_header
      toggle_xmp_feature(xmp: true)
    end

    def dimensions_from_vp8(vp8_data)
      width, height = vp8_data[6, 4]
                      .unpack('vv')
                      .map { |v| (v & 0b00111111_11111111) - 1 }
      [width, height]
    end

    def dimensions_from_vp8_lossless(vp8_data)
      b1, b2, b3, b4 = vp8_data[1, 4].unpack('c' * 4)

      width = ((b2 & 0b0011_1111) << 8) | b1
      height = ((b4 & 0b0011_1111) << 8) |
               ((b2 & 0b1100_0000) >> 6) | (b3 << 2)
      [width, height]
    end

    def dimensions_from_image_chunk(chunk_id, vp8_data)
      case chunk_id
      when VP8F_HEAD
        dimensions_from_vp8(vp8_data[0, 10])
      when VP8L_HEAD
        dimensions_from_vp8_lossless(vp8_data[0, 10])
      end
    end

    def upgrade_to_extended_format
      chunk_id, _, data = chunk(12)

      width, height = dimensions_from_image_chunk(chunk_id, data)

      vp8x_data = [VP8X_XMP_FLAG, 0, 0, 0,
                   width,  width  >> 8, 0,
                   height, height >> 8, 0].pack('c' * 10)

      chunk = new_chunk(VP8X_HEAD, vp8x_data)

      insert_into_file(12, chunk)
    end

    def toggle_xmp_feature(xmp: true)
      chunk_id, _, data = chunk(12)

      if VP8X_HEAD != chunk_id
        upgrade_to_extended_format
        return toggle_xmp_feature(xmp: xmp)
      end

      data[0] = (data.unpack1('c').to_i |  VP8X_XMP_FLAG).chr if xmp
      data[0] = (data.unpack1('c').to_i & ~VP8X_XMP_FLAG).chr unless xmp

      replace_chunk(12, chunk_id, data)
    end
  end
end
