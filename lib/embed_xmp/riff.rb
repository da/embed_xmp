# frozen_string_literal: true

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

require 'embed_xmp/image_file'

class EmbedXMP
  # Resource Interchange File Format (container format for WebP)
  class RIFF < ImageFile
    RIFF_HEAD = 'RIFF'

    # Return the RIFF file header.
    def file_header
      riff_id, file_length, data = chunk(0)
      form_type = data[0, 4]
      real_file_length = @image_data.length

      raise 'NoRIFFHeader' if RIFF_HEAD != riff_id
      raise 'FileHeaderLongerThanFile' if real_file_length != file_length

      [riff_id, file_length, form_type]
    end

    # Updates the file length value in the WebP file header.
    def update_file_length_header
      @image_data[4, 4] = [@image_data.length - 8].pack('V')
    end

    # rubocop: disable Metrics/AbcSize
    # Return chunk at +offset+ from the beginning of the file.
    def chunk(offset)
      raise 'ChunksMustBeTwoBytesAligned' if offset.odd?

      chunk_id = @image_data[offset, 4]

      data_length = @image_data[offset + 4, 4].b.unpack1('V')
      chunk_length = data_length + (data_length % 2) + 8

      raise 'ChunkExceedsFileLength' if offset + chunk_length > @image_data.length

      data = @image_data[offset + 8, data_length]

      [chunk_id, chunk_length, data]
    end
    # rubocop: enable Metrics/AbcSize

    # Remove the chunk at +offset+ from the beginning of the file.
    def remove_chunk(offset)
      _, chunk_length, = chunk(offset)

      @image_data.slice!(offset, chunk_length)

      update_file_length_header
    end

    # Replace the chunk at +offset+ from the beginning of the file
    # with a new chunk
    def replace_chunk(offset, chunk_id, data)
      remove_chunk(offset)

      chunk = new_chunk(chunk_id, data)

      insert_into_file(offset, chunk)

      update_file_length_header
    end

    # Create a new RIFF chunk with +data+ with pad byte when needed.
    def new_chunk(chunk_id, data)
      unless chunk_id.match?(/[a-zA-Z0-9 ]{4}/)
        raise 'RIFFChunkIdentifierMustBeFourChar'
      end

      data_length = data.length
      data += '\0'.b if data_length.odd?

      chunk_id.b + [data_length].pack('V') + data
    end
  end
end
