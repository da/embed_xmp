**embed_xmp** is a Ruby gem that lets you embed Extensible Metadata (XMP) (ISO 16684-1:2019) sidecar files into image files. It can be useful if you want to embed licensing, copyright, descriptions, accessive information, or other metadata into your photo files for distribution on the web and other digital channels.

It will not help you author XMP files, usually provided along with your photo files, and it will not validate the XMP data you try to embed.
It will ensure that the XML is well-formatted, however. XMP is embedded as-is, except for non-destructive whitespace reduction to keep file sizes small.

The gem is not aware of other metadata formats and will not reconcile them with the XMP data.
You must either remove other metadata formats from the file, or reconcile their records with the XMP data you embed.

Supported image formats:

 * JPEG
 * PNG
 * SVG
 * WebP

The XMP sidecar will be wrapped in an XPACKET when embedded.

## Get started

You probably want to install it via [RubyGems](https://rubygems.org/gems/embed_xmp):

```sh
gem install embed_xmp
```

To build and install the gem:

```sh
gem build embed_xmp.gemspec
gem install embed_xmp*.gem
```


## Usage

Command line:

```sh
embed_xmp sidecar.xmp input.png output.png

```

In Ruby:

```Ruby
require 'embed_xmp'

# Embed a sidecar file into a PNG image.
EmbedXMP::PNG.join_sidecar(xmp_sidecar_path, input_path, output_path)

# Copy XMP data in a string into a JPEG image.
xmp_data = "<x:xmpmeta/>"
EmbedXMP::JPEG.join_sidecar(xmp_data, input_path, output_path)
```

Documentation on [RubyDoc](https://www.rubydoc.info/gems/embed_xmp).
