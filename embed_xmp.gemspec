# frozen_string_literal: false

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

Gem::Specification.new do |s|
  s.name = 'embed_xmp'
  s.version = '0.1.2'
  s.date = '2021-01-10'
  s.summary = 'Embed XMP sidecars into image files.'
  s.description = 'Embed Extensible Metadata (XMP) sidecar files into image files.'
  s.description << ' You must supply your own XMP sidecar files, usually provided along with your photos,'
  s.description << ' this gem won’t help you write or validate them.'
  s.description << ' The gem can be useful if you want to embed licensing, copyright, descriptions,'
  s.description << ' accessive information, or other metadata into your photos.'
  s.description << ' Supports JPEG, PNG, SVG, and WebP.'
  s.homepage = 'https://www.ctrl.blog/entry/embed-xmp.html'
  s.authors = 'Daniel Aleksandersen'
  s.email = 'code@daniel.priv.no'
  s.bindir = 'bin'
  s.executables = 'embed_xmp'
  s.files = %w[bin/embed_xmp lib/embed_xmp.rb]
  s.files += Dir['lib/*/*.rb']
  s.license = 'BSD-3-Clause'
  s.extra_rdoc_files = %w[LICENSES/BSD-3-Clause.txt]
  s.required_ruby_version = '>= 2.4'
  s.add_runtime_dependency 'digest-crc', '>= 0.5.1'
  s.add_runtime_dependency 'nokogiri', '>= 1.10'
  s.metadata = {
    'bug_tracker_uri' => 'https://codeberg.org/da/embed_xmp/issues',
    'source_code_uri' => 'https://codeberg.org/da/embed_xmp'
  }
end
